# WordPress Plugin to remove heading 1 and heading 2 from TinyMCE Editor

## What does this do?

When you use TinyMCE this plugin removes the h1 and h2 options.  We're using h1 for the blog title and h2 for the post title, so we don't want users to be able to create headings at those levels.

## How does it work?

On the TinyMCE init hook this sets the allowed block formats to p, h3, h4, h5, h6 and pre

## Install
Clone to the WordPress plugins directory.  For example

```
$ cd /var/www/html/wp-content/plugins/
$ git clone git@gitlab.ebi.ac.uk:ebiwd/wordpress-tinymce-hide-headings.git
```

Log into WordPress as an administrator, go to plugins and activate this plugin.

