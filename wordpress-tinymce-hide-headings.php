<?php
/*
Plugin Name: TinyMCE Hide Headings 
Plugin URI:  https://gitlab.ebi.ac.uk/ebiwd/wordpress-tinymce-hide-headings
Description: Hide h1 and h2 tags in TinyMCE
Version:     0.0.1
Author:      EMBL-EBI Web Development
Author URI:  https://gitlab.ebi.ac.uk/ebiwd
*/

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}


add_filter('tiny_mce_before_init', 'tiny_mce_remove_unused_formats' );

/*
 * Modify TinyMCE editor to remove h1 and h2.
 */
function tiny_mce_remove_unused_formats($init) {
	// Add block format elements you want to show in dropdown
	$init['block_formats'] = 'Paragraph=p;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Preformatted=pre';
	return $init;
}

?>